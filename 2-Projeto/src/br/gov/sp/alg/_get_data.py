import requests
import os
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
import xlsxwriter
from openpyxl import workbook
from openpyxl import load_workbook



def obter_dados(doc_link):
    # RASPA OS DADOS DA PAGINA
    page_doc = requests.get(doc_link)
    # TRATA OS DADOS COM BEAUTIFUL SOUP
    soup_doc = BeautifulSoup(page_doc.content, 'html.parser')
    # COLETA OS DADOS DO DOCUMENTO
    mod_type = soup_doc.find(id='content_content_content_detalhesPregao_lblModalidade').get_text()
    pro_numb = soup_doc.find(id='content_content_content_detalhesPregao_lblProcesso').get_text()
    dat_plub = soup_doc.find(id='content_content_content_detalhesPregao_lblPublicadoEm').get_text()
    exc_plac = soup_doc.find(id='content_content_content_detalhesPregao_lblLocaldeExecucao').get_text()
    dat_open = soup_doc.find(id='content_content_content_detalhesPregao_lblAbertura').get_text()
    lbl_area = soup_doc.find(id='content_content_content_detalhesPregao_lblArea').get_text()
    lbl_sare = soup_doc.find(id='content_content_content_detalhesPregao_lblSubArea').get_text()
    obj_desc = soup_doc.find(
        id='content_content_content_detalhesPregao_lblObjetodaLicitacao').get_text()
    data[0].append('EM ANDAMENTO')
    data[1].append(mod_type)
    data[2].append(pro_numb)
    data[3].append(dat_plub)
    data[4].append(dat_open)
    data[5].append(exc_plac)
    data[6].append(lbl_area)
    data[7].append(lbl_sare)
    data[8].append(obj_desc)
    data[9].append(doc_link)

#URL DA PÁGINA DO ESTADO DE SP
url = 'https://www.imprensaoficial.com.br/eNegocios/ResultadoBuscaENegociosTransparencia.aspx?Status=2'
page_list = requests.get(url)

data = [[],[],[],[],[],[],[],[],[],[]]
data[0].append('STATUS')
data[1].append('MODALIDADE')
data[2].append('PROCESSO Nº')
data[3].append('DATA PUBLICAÇÃO')
data[4].append('DATA ABERTURA')
data[5].append('LOCAL DE EXECUÇÃO')
data[6].append('AREA')
data[7].append('SUB AREA')
data[8].append('OBJETO')
data[9].append('LINK')

#LOOP ATÉ A CONEXÃO SER ESTABELECIDA
while (page_list.status_code == 503):
    print('Sem conexão! :c')
else:
    chrome_options = Options()
    chrome_options.add_argument("headless")
    #a = "path/of/headless/chromedriver"
    chrome_options.add_argument("--disable-notifications")

    driver = webdriver.Chrome(r"C:\Users\vinic\Desktop\_Integrator_project_web_bot\resources\chromedriver.exe", options=chrome_options)
    driver.get(url)

    #OBTEM A QUANTIDADE DE PAGINAS
    qty_pag = int(driver.find_element_by_xpath('//*[@id="content_content_content_ResultadoBusca_PaginadorCima_QuantidadedePaginas"]').text)
    #OBTEM A QUANTIDADE DE DOCUMENTOS
    qty_doc = int(driver.find_element_by_xpath('//*[@id="content_content_content_filtroBuscaControle_lblDocumentosEncontrado"]').text)

    #OBTEM A MODALIDADE DA LICITAÇÃO
    doc_type = driver.find_element_by_xpath('//*[@id="content_content_content_ResultadoBusca_dtgResultadoBusca"]/tbody/tr[2]/td[3]').text

    #VARIAVEL DE PAGINAÇÃO DO NAVEGADOR
    j = 0
    c_doc = 0
    while j < qty_pag:
        while c_doc < qty_doc:
            i = 0
            if j > 0 and c_doc < qty_doc:
                while i < 10:
                    if i == 0:
                        driver.find_element(By.ID, "content_content_content_ResultadoBusca_PaginadorCima_btnProxima").click()
                        # OBTEM A MODALIDADE DA LICITAÇÃO
                        doc_type = driver.find_element_by_xpath(
                            '//*[@id="content_content_content_ResultadoBusca_dtgResultadoBusca"]/tbody/tr['+str(i+2)+']/td[3]').text
                        if doc_type == 'PREGÃO ELETRÔNICO' or doc_type == 'PREGÃO PRESENCIAL' or doc_type == 'PREGÃO':
                            doc_link = driver.find_element_by_id('content_content_content_ResultadoBusca_dtgResultadoBusca_hlkObjeto_'+str(i)).get_attribute('href')

                            #FUNCAO PARA RASPAR OS DADOS
                            obter_dados(doc_link)
                            i += 1
                            c_doc += 1
                            if i == 10 or c_doc == qty_doc:
                                j += 1
                                i = 10
                        else:
                            i += 1
                            c_doc += 1
                            if i == 10 or c_doc == qty_doc:
                                j += 1
                                i = 10
                    else:
                        # OBTEM A MODALIDADE DA LICITAÇÃO
                        doc_type = driver.find_element_by_xpath(
                            '//*[@id="content_content_content_ResultadoBusca_dtgResultadoBusca"]/tbody/tr[' + str(i + 2) + ']/td[3]').text
                        if doc_type == 'PREGÃO ELETRÔNICO' or doc_type == 'PREGÃO PRESENCIAL' or doc_type == 'PREGÃO':
                            doc_link = driver.find_element_by_id('content_content_content_ResultadoBusca_dtgResultadoBusca_hlkObjeto_'+str(i)).get_attribute('href')

                            # FUNCAO PARA RASPAR OS DADOS
                            obter_dados(doc_link)
                            i += 1
                            c_doc += 1
                            if i == 10 or c_doc == qty_doc:
                                j += 1
                                i = 10
                        else:
                            i += 1
                            c_doc += 1
                            if i == 10 or c_doc == qty_doc:
                                j += 1
                                i = 10

            elif j <= 0 and c_doc <= qty_doc:
                while i < 10:
                    # OBTEM A MODALIDADE DA LICITAÇÃO
                    doc_type = driver.find_element_by_xpath(
                        '//*[@id="content_content_content_ResultadoBusca_dtgResultadoBusca"]/tbody/tr['+str(i+2)+']/td[3]').text
                    if doc_type == 'PREGÃO ELETRÔNICO' or doc_type == 'PREGÃO PRESENCIAL' or doc_type == 'PREGÃO':
                        doc_link = driver.find_element_by_id('content_content_content_ResultadoBusca_dtgResultadoBusca_hlkObjeto_'+str(i)).get_attribute('href')

                        #FUNCAO PARA RASPAR OS DADOS
                        obter_dados(doc_link)
                        i += 1
                        c_doc += 1
                        if i == 10 or c_doc == qty_doc:
                            j += 1
                            i = 10
                    else:
                        i += 1
                        c_doc += 1
                        if i == 10 or c_doc == qty_doc:
                            j += 1
                            i = 10
            else:
                if c_doc >= qty_doc:
                    break
        j = j + 1

#ENCERRA O NAVEGADOR
driver.close()
i=0
j=0

excel = load_workbook(r"C:\Users\vinic\Desktop\_Integrator_project_web_bot\src\br\gov\sp\alg\licitacoes.xlsx")
plan1 = excel.active

for row in range(0,len(data[0])):
    for col in range(0,10):
        v = data[col][row]
        plan1.cell(column=col+1, row=row+27, value="{}".format(v))

excel.save(r"C:\Users\vinic\Desktop\_Integrator_project_web_bot\src\br\gov\sp\alg\licitacoes.xlsx")

os.system('start "excel" "C:\\Users\\vinic\\Desktop\\_Integrator_project_web_bot\\docs\\licitacoes_.xlsx"')

