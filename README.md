[![LICITA-BOT](1-Documentos/Licita-bot.png "LICITA-BOT")

## LICITA -BOT
O Licita-bot é um web-bot tem o propósito simplificar as consultas que são executadas no portal da transparência do governo do estado de São Paulo de uma maneira fácil e dinâmica.
Ele foi especialmente pensando para os usuários que desejam informações de licitações abertas cuja modalidade seja: 
- Pregão
- Pregão eletrônico
- Pregão presencial

O Licita-bot é um web-bot tem o propósito simplificar as consultas que são executadas no portal da transparência do governo do estado de São Paulo de uma maneira fácil e dinâmica.

## OUTPUT
Os dados coletados pelo licita-bot são salvo em um planilha Excel que está configurada, de modo a permitir que o usuário consiga filtrar os dados.
Os dados coletados pelo licita-bot são:

- Status
- Modalidade
- Processo nº
- Data da publicação
- Data da abertura
- Local de execução
- Área
- Sub-area
- Objeto
- URL


## MAS COMO O LICITA-BOT FUNCIONA!
O processo de obtenção dos dados consiste basicamente em:
- Acessar a página do governo do estado de São Paulo
- Avaliar a modalidade das licitações

[![FLUXO](1-Documentos/Diagram.png "FLUXO")](https://gitlab.com/viniciusgabriel.oliveira/webbot/blob/_Final_project/1-Documentos/Diagram.png "FLUXO")

## PRÉ-REQUISITOS!
- Google Chrome
- Excel ou Libre-Office
- Python 3.x

## INSTALAÇÃO
- $ pip install -r requirements.txt

## RUN
- $ Python3 Projeto/src/br/gov/sp/alg/_get_data.py


## FERRAMENTAS UTILIZADAS
Foi utilizada linguagem de programação Python, à partir da versão 3.7.x , que pode ser baixado através do site:  https://www.python.org/downloads/  
 Foram utilizadas as seguintes bibliotecas do Python, que foram instaladas através do comando "pip install nome_biblioteca":  

- Selenium 
- Beautiful Soup 4 (bs4) 
-  Requests 
-  Openpyxl

Instalar o "Chromedriver", de acordo com a versão do Google Chrome instalado no equipamento, através do link:   https://chromedriver.chromium.org/downloads 

A IDE utilizada foi o Pycharm, que pode ser realizado download em:  https://www.jetbrains.com/pycharm/download/ .

Sistema Operacional:
- Windows 10


